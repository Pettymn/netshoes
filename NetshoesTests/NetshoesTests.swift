//
//  NetshoesTests.swift
//  NetshoesTests
//
//  Created by Peterson Mauricio on 23/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import XCTest
import Alamofire

@testable import Netshoes

class NetshoesTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let headers = [
            "User-Agent": "Netshoes App",
            "X-Requested-With": "XMLHttpRequest"
        ]
        
        let URLString = "\(CommonManager.CONST_URLS.BASE_URL)/departamento/running/tenis?mi=hm_ger__mntop__RUN-tenis"
        var request: NSURLRequest?
        var responses: NSHTTPURLResponse?
        var error: NSError?
        
        let expectation = expectationWithDescription("Download request should download data to file: \(URLString)")
        
        Alamofire.request(.GET,
            URLString,
            headers: headers)
            .responseJSON { response in
                if response.result.isSuccess {
                    
                    request = response.request
                    responses = response.response
                    error = response.result.error
                    
                    expectation.fulfill()
                }else{
                    XCTAssertFalse(true)
                }
        }
        
        
        waitForExpectationsWithTimeout(10.0, handler: nil)
        
        XCTAssertNotNil(request, "request should not be nil")
        XCTAssertNotNil(responses, "response should not be nil")
        XCTAssertNil(error, "error should be nil")
        
        
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
