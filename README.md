##Peterson Mauricio

Para este projeto foi utilizada a arquitetura MVVM (Model-View-ViewModel).
Desde o lançamento de Swift e com sua nova filosofia (Protocol-Oriented)
achei que já era hora de sair do tão famoso MVC muito utilizado em Obj-c;

A comunidade iOS que sigo tem um amor especial por um determinado blog, e usei ele como referiencia:
https://www.natashatherobot.com/swift-mvvm-optionals/

Utilizei algumas dependências e um gerenciador para tal tarefa.
- Cocoapods
- Alamofire
- JSONSwift

Obs.: A URL que recebi para acessar os dados dos produtos não estava funcionando,  então usei outra que tinha no site.

Abaixo, um vídeo da execução do app:

![alt tag](https://bytebucket.org/Pettymn/netshoes/raw/74ff75c3bd9d0b359ea6d3b8bc55f86bc77cd3eb/video.gif)

