//
//  TitleDetailCell.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

class TitleDetailCell: UITableViewCell {
    
    @IBOutlet weak var productName: UILabel!
    
    func configure(withName name:String){
        self.productName.text = name
    }
 
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
}
