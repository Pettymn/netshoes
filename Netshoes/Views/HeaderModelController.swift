//
//  HeaderModelController.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

class HeaderModelController: NSObject,UIPageViewControllerDataSource{
    
    var pageData = NSArray()
    var _HeaderDetailCell:HeaderDetailCell?
    
    func viewControllerAtIndex(index:NSInteger, storyboard:UIStoryboard) ->ProductHeaderViewController?{
        
        if (self.pageData.count == 0 || index >= self.pageData.count){
            return nil
        }
        
        let _ProductHeaderViewController = storyboard.instantiateViewControllerWithIdentifier(String(ProductHeaderViewController)) as? ProductHeaderViewController
        
        _ProductHeaderViewController!.image = self.pageData[index] as? Image
        _ProductHeaderViewController!.index = index
        _ProductHeaderViewController!.delegate = _HeaderDetailCell
        
        return _ProductHeaderViewController
    }
    func indexOfViewController(viewController:ProductHeaderViewController) ->NSInteger{
        return self.pageData.indexOfObject(viewController.image!)
    }
    
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        var indexs = self.indexOfViewController(viewController as! ProductHeaderViewController)
        
        if indexs == 0 || indexs == NSNotFound {
            return nil
        }
        indexs--
        return self.viewControllerAtIndex(indexs, storyboard: viewController.storyboard!)
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?{
        var indexs = self.indexOfViewController(viewController as! ProductHeaderViewController)
        
        if indexs == NSNotFound{
            return nil
        }
        indexs++
        
        return self.viewControllerAtIndex(indexs, storyboard: viewController.storyboard!)
    }
    
}
