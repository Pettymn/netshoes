//
//  ProductCell.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    
    func configure(withProduct product:Product){
        
        self.layer.borderWidth = 0.1
        self.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        if let name = product.name{
            self.productName.text = name
        }
        
        self.productPrice.attributedText = product.price?.formmatedString
        
        if let path = product.image?.normal {
            let url = NSURL(string: "https:\(path)")!
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                let data = NSData(contentsOfURL: url)!
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let img = UIImage(data: data)
                    self.productImage.image = img
                })
                
            })
        }
    }
    
}
