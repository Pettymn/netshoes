//
//  HeaderDetailCell.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

class HeaderDetailCell: UITableViewCell ,UIPageViewControllerDelegate,HeaderProtocol{
    
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var pageViewController:UIPageViewController?
    var modelController:HeaderModelController?
    var mainViewController:UIViewController?
    var currentIndex = 0
    
    var gallery = [Image]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.mainViewController = UIViewController()
        self.mainViewController!.view.frame = self.containerView.bounds
        self.containerView.addSubview(self.mainViewController!.view)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    func configure(withGallery images:[Image]?){
        if let gallerys = images {
            self.gallery = gallerys
            
            if self.modelController == nil {
                self.pageControl.currentPage = currentIndex
                self.pageControl.numberOfPages = gallerys.count
                
                self.modelController = self.modelController == nil ? HeaderModelController():self.modelController
                self.modelController!.pageData = gallerys
                self.modelController!._HeaderDetailCell = self
                
                setupPageViewController()
            }
            
        }
    }
    
    func setupPageViewController(){
        
        self.pageViewController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        self.pageViewController!.delegate = self
        
        let startViewController = self.modelController?.viewControllerAtIndex(0,storyboard: UIStoryboard(name: "Main", bundle: nil))
        
        startViewController!.delegate = self
        
        var viewControllers = [ProductHeaderViewController]()
        viewControllers.append(startViewController!)
        
        self.pageViewController!.setViewControllers(viewControllers, direction: .Forward, animated: false, completion: nil)
        self.pageViewController!.view.frame = self.frame
        self.pageViewController!.didMoveToParentViewController(self.mainViewController)
        self.pageViewController!.dataSource = self.modelController
        
        self.mainViewController!.addChildViewController(self.pageViewController!)
        self.mainViewController!.view.addSubview(self.pageViewController!.view)
        
        
    }
    
    
    // MARK: - HeaderProtocol
    func viewAtIndex(index index: NSInteger) {
        self.currentIndex = index
        self.pageControl.currentPage = index
    }
    
}
