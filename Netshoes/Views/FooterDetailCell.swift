//
//  FooterDetailCell.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

class FooterDetailCell: UITableViewCell {
    
    @IBOutlet weak var productPrice: UILabel!
    
    func configure(withPrice price:Price){
        if price.original_price!.characters.count > 0{
            let formmated = "\(price.percentage) \(price.original_price!)\n \(price.actual_price!)" as NSString
            self.productPrice.attributedText = formmated.attributedPercentage(price.percentage, original_price: price.original_price!)
        }else{
            self.productPrice.text = price.actual_price!
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.productPrice.setNeedsLayout()
        self.productPrice.setNeedsDisplay()
    }
    
}
