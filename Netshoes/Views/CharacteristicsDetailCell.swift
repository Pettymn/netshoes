//
//  CharacteristicsDetailCell.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

class CharacteristicsDetailCell: UITableViewCell {
    
    
    @IBOutlet weak var productCharacteristics: UILabel!
    
    func configure(withCharacteristics product:Product){
        
        if product.characteristics != nil {
            var str_characteristics = "Descrição\n \(product.description!)\n\n"
            var objects = [String]()
            objects.append("Descrição")
            for _c in product.characteristics!{
                str_characteristics = str_characteristics + "\(_c.name!)\n\(_c.value!)\n\n"
                objects.append(_c.name!)
            }
            self.productCharacteristics.attributedText = (String(str_characteristics.characters.dropLast(2)) as NSString).attributedObjects(objects)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.productCharacteristics.setNeedsLayout()
        self.productCharacteristics.setNeedsDisplay()
    }
}
