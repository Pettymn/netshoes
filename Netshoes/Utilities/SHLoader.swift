//
//  SHLoader.swift
//  SH
//
//  Created by Pettymn on 05/10/15.
//  Copyright © 2015 Peterson Mauricio. All rights reserved.
//

import UIKit

public class SHLoader: UIView {
    
    // MARK: - Singleton
    
    
    
    public class var sharedSingleton: SHLoader{
        struct Singleton {
            static let instance = SHLoader(frame: CGRect.zero)
        }
        return Singleton.instance
    }
    
    // MARK: - Private interface
    
    //
    // layout elements
    //
    
    private var blurEffectStyle: UIBlurEffectStyle = .Dark
    private var blurEffect: UIBlurEffect!
    private var blurView: UIVisualEffectView!
    private var vibrancyView: UIVisualEffectView!
    private let numberOfSprites = 8
    
    // MARK: - Init
    
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        blurEffect = UIBlurEffect(style: blurEffectStyle)
        blurView = UIVisualEffectView(effect: blurEffect)
        addSubview(blurView)
        
        vibrancyView = UIVisualEffectView(effect: UIVibrancyEffect(forBlurEffect: blurEffect))
        addSubview(vibrancyView)
        
        let window = UIApplication.sharedApplication().windows.first!
//        print(window.frame)
//        
//        let shImageView = UIImageView(frame: CGRectMake((window.frame.size.width / 2)-25, (window.frame.size.height / 2)-25, 50, 50))
//        shImageView.contentMode = UIViewContentMode.Center
//        var spriteArray = [UIImage]()
//        
//        for(var i = 0; i < numberOfSprites; i++) {
//            let image = UIImage(named: "s\(i+1)")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysTemplate)
//            spriteArray.append(image!)
//        }
//        
//        shImageView.animationImages = spriteArray
//        shImageView.animationDuration = 0.8
//        blurView.contentView.addSubview(shImageView)
//        shImageView.startAnimating()
        
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
        indicator.center = CGPointMake((window.frame.size.width / 2), (window.frame.size.height / 2))
        
        blurView.contentView.addSubview(indicator)
        indicator.startAnimating()
        
        userInteractionEnabled = true
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Show Annimation Methods
    
    
    public class func show() ->SHLoader{
        
        let window = UIApplication.sharedApplication().windows.first!
        let loader = SHLoader.sharedSingleton
        
        loader.updateFrame()
        
        
        if loader.superview == nil {
            
            // show the loader
            loader.alpha = 0.0
            window.addSubview(loader)
            
            UIView.animateWithDuration(0.1, delay: 0.0, options: [.CurveEaseOut], animations: { () -> Void in
                loader.alpha = 1.0
                }, completion: nil)
        }
        
        return loader
        
    }
    
    
    
    public class func hide() {
        let loader = SHLoader.sharedSingleton
        dispatch_async(dispatch_get_main_queue(),{
            if loader .superview == nil {
                return
            }
            UIView.animateWithDuration(0.33, delay: 0.0, options: [.CurveEaseOut], animations: { () -> Void in
                loader.alpha = 0.0
                }, completion: { (finished) -> Void in
                    loader.alpha = 1.0
                    loader.removeFromSuperview()
            })
        })
    }
    
    
    //
    // observe the view frame and update the subviews layout
    //
    public override var frame: CGRect {
        didSet {
            if frame == CGRect.zero {
                return
            }
            blurView.frame = bounds
            vibrancyView.frame = blurView.bounds
        }
    }
    
    
    public func updateFrame(){
        let window = UIApplication.sharedApplication().windows.first!
        SHLoader.sharedSingleton.frame = window.frame
    }
    
}