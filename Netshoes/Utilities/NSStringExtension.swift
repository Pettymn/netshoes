//
//  NSStringExtension.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

extension NSString {
    
    func attributedPercentage(percentage:String,original_price:String) -> NSMutableAttributedString{
        let mutableAttributedString = NSMutableAttributedString(string: self as String)
        
        let pRange = self.rangeOfString(percentage)
        mutableAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.whiteColor(), range: pRange)
        mutableAttributedString.addAttribute(NSBackgroundColorAttributeName, value: UIColor.orangeColor(), range: pRange)
        
        let oRange = self.rangeOfString(original_price)
        mutableAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: oRange)
        mutableAttributedString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: oRange)
        
        return mutableAttributedString
    }
    
    func attributedPrice(original_price:String) -> NSMutableAttributedString{
        let mutableAttributedString = NSMutableAttributedString(string: self as String)
        
        let oRange = self.rangeOfString(original_price)
        mutableAttributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGrayColor(), range: oRange)
        mutableAttributedString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: oRange)
        
        return mutableAttributedString
    }
    
    func attributedObjects(objects:[String]) -> NSMutableAttributedString{
        let mutableAttributedString = NSMutableAttributedString(string: self as String)
        for _str in objects {
            let oRange = self.rangeOfString(_str)
            mutableAttributedString.addAttributes([NSFontAttributeName:UIFont.boldSystemFontOfSize(17)], range: oRange)

        }
        return mutableAttributedString
    }
    
}
