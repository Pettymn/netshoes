//
//  Product.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit
import JSONHelper

class Product: Deserializable {
    
    var name:String?
    var url:String?
    var price:Price?
    var image:Image?
    var description:String?
    var galerry:[Image]?
    var characteristics:[Characteristic]?
    
    
    
    required init(data: JSONDictionary) {
        name <-- data["name"]
        url <-- data["url"]
        price <-- data["price"]
        image <-- data["image"]
        
        description <-- data["description"]
        characteristics <-- data["characteristics"]
        
        if (data["gallery"] != nil){
            do {
                let jsonData = try NSJSONSerialization.dataWithJSONObject(data["gallery"]![0].objectForKey("items")!, options: NSJSONWritingOptions.PrettyPrinted)
                let string = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                galerry <-- string
            }catch let error as NSError{
                print(error.description)
            }
        }
        
    }
    
}
