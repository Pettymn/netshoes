//
//  Price.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit
import JSONHelper

class Price: Deserializable {
    
    var actual_price:String?
    var from_price:String?
    var original_price:String?
    var payment_condition:String?
    var saving:String?
    
    var descriptionPrice:String{
        return "actual_price:\(actual_price)\n from_price:\(from_price)\n original_price:\(original_price)\n payment_condition:\(payment_condition)\n saving:\(saving)\n"
    }
    
    var formmatedString:NSMutableAttributedString{
        if original_price!.characters.count > 0 {
            let str = "\(actual_price!) \(original_price!)" as NSString
            return str.attributedPrice(original_price!)
        }else{
            return NSMutableAttributedString(string: actual_price!)
        }
    }
    
    var percentage:String{
        
        let actualArray = actual_price!.componentsSeparatedByCharactersInSet(
            NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        let actualString = actualArray.joinWithSeparator("")
        
        let originalArray = original_price!.componentsSeparatedByCharactersInSet(
            NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        let originalString = originalArray.joinWithSeparator("")
        
        let percent = ((NSInteger(actualString)! * 100) / NSInteger(originalString)! ) - 100
        return "\(percent)%"
    }
    
    
    required init(data: JSONDictionary) {
        actual_price <-- data["actual_price"]
        from_price <-- data["from_price"]
        original_price <-- data["original_price"]
        payment_condition <-- data["payment_condition"]
        saving <-- data["saving"]
        
        //        print(descriptionPrice)
    }
    
}
