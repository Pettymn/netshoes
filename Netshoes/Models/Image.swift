//
//  Image.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit
import JSONHelper

class Image: Deserializable {
    
    var large:String?
    var medium:String?
    var normal:String?
    var small:String?
    var thumb:String?
    var zoom:String?
    
    required init(data: JSONDictionary) {
        large <-- data["large"]
        medium <-- data["medium"]
        normal <-- data["normal"]
        small <-- data["small"]
        thumb <-- data["thumb"]
        zoom <-- data["zoom"]
    }
}
