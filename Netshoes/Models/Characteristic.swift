//
//  Characteristic.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit
import JSONHelper

class Characteristic: Deserializable {
    
    var name:String?
    var value:String?
    
    required init(data: JSONDictionary) {
        name <-- data["name"]
        value <-- data["value"]
    }
    
}
