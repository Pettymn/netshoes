//
//  ProductHeaderViewController.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

protocol HeaderProtocol{
    func viewAtIndex(index index:NSInteger)
}

class ProductHeaderViewController: UIViewController {
    
    // MARK: - Properties
    var image:Image?
    var index:NSInteger?
    var delegate:HeaderProtocol?
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    // MARK: - UIView Lifecyle
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImage()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.delegate?.viewAtIndex(index: index!)
    }
    
    // MARK: - Load Image Methods
    
    func loadImage(){
        
        if let path = image?.large {
            let url = NSURL(string: "https:\(path)")!
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), { () -> Void in
                let data = NSData(contentsOfURL: url)!
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    let img = UIImage(data: data)
                    self.productImage.image = img
                    self.activityIndicator.stopAnimating()
                })
                
            })
        }
        
    }
    
    
}
