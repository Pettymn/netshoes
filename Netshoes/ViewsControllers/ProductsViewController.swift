//
//  ProductsCollectionViewController.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    // MARK: - Properties
    var products = [Product]()
    var productViewModel = ProductViewModel()
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadProducts()
    }
    
    // MARK: - Data Methods
    
    func loadProducts(){
        SHLoader.show()
        productViewModel.allProducts({ (products) -> () in
            self.products = products!
            self.collectionView.reloadData()
            SHLoader.hide()
            }) { (error) -> () in
                SHLoader.hide()
                self.showAlert(error)
        }
    }
    
    func collectionView(collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
            return CGSize(width: 150, height: 224)
    }
    // MARK: - UICollectionViewDataSource
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier(String(ProductCell), forIndexPath: indexPath) as? ProductCell {
            let product = products[indexPath.row]
            cell.configure(withProduct: product)
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let product = products[indexPath.row]
        self.performSegueWithIdentifier("idSegueShowDetail", sender: product)
    }
    
    // MARK: - Segue Methods
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "idSegueShowDetail" {
            if let viewController = segue.destinationViewController as? ProductDetailTableViewController {
                viewController.product = sender as! Product
            }
        }
    }
    
    // MARK: - Alert
    
    func showAlert(message:String){
        let alertController = UIAlertController(title: "Ops!", message: message, preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Novamente", style: .Default, handler: { (alert) -> Void in
            self.loadProducts()
        }))
        alertController.addAction(UIAlertAction(title: "Entendido", style: .Cancel, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
}
