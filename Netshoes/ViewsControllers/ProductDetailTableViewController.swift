//
//  ProductDetailTableViewController.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit

class ProductDetailTableViewController: UITableViewController {
    
    
    
    
    
    var product:Product!
    var productViewModel = ProductViewModel()
    
    private enum ProductDetails: Int {
        case Header,Title,Characteristics,Footer
    }
    
    // MARK: - UIView Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.separatorStyle = .None
        
        SHLoader.show()
        productViewModel.details(product,
            successBlock: { (product) -> () in
                if (product != nil) {
                    self.product = product!
                    self.tableView.reloadData()
                    self.tableView.setNeedsLayout()
                    self.tableView.layoutIfNeeded()
                    SHLoader.hide()
                }
            }) { (error) -> () in
                SHLoader.hide()
        }
    }
    
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch ProductDetails(rawValue: indexPath.row)!{
            
        case .Header:
            let cell = tableView.dequeueReusableCellWithIdentifier(String (HeaderDetailCell), forIndexPath: indexPath) as! HeaderDetailCell
            cell.configure(withGallery:self.product.galerry)
            return cell
        case .Title:
            let cell = tableView.dequeueReusableCellWithIdentifier(String (TitleDetailCell), forIndexPath: indexPath) as! TitleDetailCell
            cell.configure(withName: self.product.name!)
            return cell
        case .Characteristics:
            let cell = tableView.dequeueReusableCellWithIdentifier(String (CharacteristicsDetailCell), forIndexPath: indexPath) as! CharacteristicsDetailCell
            cell.configure(withCharacteristics: self.product)
            return cell
        case .Footer:
            let cell = tableView.dequeueReusableCellWithIdentifier(String (FooterDetailCell), forIndexPath: indexPath) as! FooterDetailCell
            cell.configure(withPrice: self.product.price!)
            return cell
        }
        
    }
    
}
