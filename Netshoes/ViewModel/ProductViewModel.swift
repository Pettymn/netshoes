//
//  ProductViewModel.swift
//  Netshoes
//
//  Created by Peterson Mauricio on 24/02/16.
//  Copyright © 2016 Peterson Mauricio. All rights reserved.
//

import UIKit
import Alamofire
import JSONHelper


class ProductViewModel: BaseViewModel {
    
    
    let headers = [
        "User-Agent": "Netshoes App",
        "X-Requested-With": "XMLHttpRequest"
    ]
    
    func allProducts(successBlock:(products:[Product]?)->(),failureBlock:(error:String)->()){
        
        Alamofire.request(.GET,
            "\(CommonManager.CONST_URLS.BASE_URL)/departamento/running/tenis?mi=hm_ger__mntop__RUN-tenis",
            headers: headers)
            .responseJSON { response in
                if response.result.isSuccess {
                    // Que API bem filha de uma puta,  essa!!!
                    if let products = response.result.value!["value"]!!["products"]!{
                        do {
                            let jsonData = try NSJSONSerialization.dataWithJSONObject(products, options: NSJSONWritingOptions.PrettyPrinted)
                            let string = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                            
                            var products = [Product]()
                            products <-- string
                            successBlock(products: products)
                            
                        }catch let error as NSError{
                            print(error.description)
                            failureBlock(error: "Ei querido, veio um negócio meio estranho aqui e não estou conseguindo prosseguir!")
                        }
                    }else{
                        failureBlock(error: "Ei querido, teremos que continuar mais tarde. Estou com problemas aqui!")
                    }
                }else{
                    failureBlock(error: "Ei querido, você precisa de uma tal de INTERNET para prosseguir!")
                }
        }
        
    }
    
    func details(var product:Product,successBlock:(product:Product?)->(),failureBlock:(error:String)->()){
        
        Alamofire.request(.GET,
            "\(CommonManager.CONST_URLS.BASE_URL)\(product.url!)",
            headers: headers)
            .responseJSON { response in
                if response.result.isSuccess {
                    if let products = response.result.value!["value"]!{
                        do {
                            let jsonData = try NSJSONSerialization.dataWithJSONObject(products, options: NSJSONWritingOptions.PrettyPrinted)
                            let string = NSString(data: jsonData, encoding: NSUTF8StringEncoding) as! String
                            product <-- string
                            successBlock(product: product)
                        }catch let error as NSError{
                            print(error.description)
                            failureBlock(error: "Ei querido, veio um negócio meio estranho aqui e não estou conseguindo prosseguir!")
                        }
                    }else{
                        failureBlock(error: "Ei querido, teremos que continuar mais tarde. Estou com problemas aqui!")
                    }
                }else{
                    failureBlock(error: "Ei querido, você precisa de uma tal de INTERNET para prosseguir!")
                }
                
        }
        
        
        
    }
    
    
}
